function [region] = create_pml(pml_grid, pml_length,L,omega_norma,plot_pml)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

scale_factor = 80;

% X_dash = linspace(0,L,N);
% X_dash_mag = step_size/2:step_size:L-step_size/2;
N = length(pml_grid);
pml_range = pml_grid(1:pml_length)-pml_grid(pml_length);
pml_end = pml_grid(end-pml_length+1:end)-pml_grid(end-pml_length+1);
% pml_range_mag = X_dash_mag(1:pml_length);

pml_area = 1 + (scale_factor*pml_range/L).^2/(1i*omega_norma);
pml_area_end = 1 + (scale_factor*pml_end/L).^2/(1i*omega_norma);
% pml_area_mag = 1 + (scale_factor*pml_range_mag/L).^2/(1i*omega_norma);

region = [(pml_area) ones(1,N-2*pml_length) pml_area_end]; 


if(plot_pml)
    figure();
    subplot(2,1,1);
    plot(-imag(region));
    title('stretching function');

    subplot(2,1,2);
    plot(abs(region));
    title('Absolute stretching function');
end

end

