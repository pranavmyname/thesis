function [f_laplace,A,region_electric,region_mag,eig_val,eig_vec] =...
    evaluate_A_ind(N,pml_length,M,omega,q,L,step_size,sigma,slab_width,c,pml_strength)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
omega_norma = (c)^(-1)*L*omega;



%% PML

X_dash = step_size:step_size:L-step_size;
X_dash_mag = step_size/2:step_size:L-step_size/2;
% N = length(X_dash);

pml_range = X_dash(1:pml_length);
pml_range_mag = X_dash_mag(1:pml_length);
if(nargin<11)
    pml_strength = 120;
end
shi_y = (pml_strength*pml_range/L).^(2);
shi_y_mag = (pml_strength*pml_range_mag/L).^(2);

pml_area = 1 + shi_y./(1i*omega_norma);
pml_area_mag = 1 + shi_y_mag./(1i*omega_norma);

% region_electric = [fliplr(pml_area) ones(1,N-2*pml_length) pml_area]; 
% region_mag = [fliplr(pml_area_mag) ones(1,N+1-2*pml_length) pml_area_mag];

region_electric = [fliplr(shi_y) zeros(1,N-2*pml_length) shi_y]; 
region_mag = [fliplr(shi_y_mag) zeros(1,N+1-2*pml_length) shi_y_mag];


M_chi = spdiags([region_electric(:); region_mag(:)],0,2*N+1,2*N+1);
% M_chi = sparse([1:N N+1:2*N+1 2*N+2:3*N+1],[1:N N+1:2*N+1 1:N],...
%     [region_electric(:); region_mag(:); -region_electric(:)],3*N+1,3*N+1);

%% S Matrix

% S = spdiags([zeros(floor(N/2)-1,1); sigma*ones(slab_width,1)],...
%     0,3*N+1,3*N+1);
S = sparse(floor(N/2):floor(N/2)+slab_width-1,...
    floor(N/2):floor(N/2)+slab_width-1,sigma*ones(slab_width,1), ...
    2*N+1,2*N+1);


%% D Matrix

D = sparse([N+1:2*N N+2:2*N+1 1:N 1:N],[1:N 1:N N+1:2*N N+2:2*N+1],[(step_size)^(-1)*ones(N,1)...
    -(step_size)^(-1)*ones(N,1) -(step_size)^(-1)*ones(N,1) (step_size)^(-1)*ones(N,1)], 2*N+1,2*N+1)*L;


%% Assign to base

assignin('base','D',D);


%% Output

% D + (1+chi(x)/s))*M
% tf_system = D+(M_chi/(1i*omega_norma)+speye(length(M_chi)))*M*1i*omega_norma;
% f_laplace = -tf_system\q;
% A = ((M\D)+M_chi);
tf_system = (D+S+M_chi+1i*omega_norma*M);
f_laplace = -tf_system\q;
A = M\(D+S+M_chi);
disp(['nrgout = ' num2str(nargout)]);
if(nargout>5)
%     [eig_vec,eig_val] = eig(A);
    [eig_vec,eig_val] = eigs(A,2*N+1);
    eig_val = diag(eig_val);
elseif(nargout>4)
    eig_val = eigs(A,2*N+1);
end


                                        
end

