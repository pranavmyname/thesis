figure()
bg = uibuttongroup('Position',[0 0 .2 1],...
                  'SelectionChangedFcn',@bselection);
              
% Create three radio buttons in the button group.
r1 = uicontrol(bg,'Style',...
                  'radiobutton',...
                  'String','Option 1',...
                  'Position',[10 350 100 30]);
              
r2 = uicontrol(bg,'Style','radiobutton',...
                  'String','Option 2',...
                  'Position',[10 250 100 30]);

r3 = uicontrol(bg,'Style','radiobutton',...
                  'String','Option 3',...
                  'Position',[10 150 100 30]);
              
% Make the uibuttongroup visible after creating child objects. 
bg.Visible = 'on';
r2.Callback = @(es,ed) disp(es.Value);

    function bselection(source,event)
%        disp(['Previous: ' source.Value]);
        event.NewValue
       disp(['Current: ' event.NewValue.String]);
       disp(event.NewValue.Value);
       disp('------------------');
    end