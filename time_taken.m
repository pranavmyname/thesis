function [t,f] = time_taken(A,omega_norma,b)
%time_taken Measures the time taken to solve Ax = b
%Additional output x can be used for the solution
tic;
f = (A+1i*omega_norma*speye(length(A)))\b;
t = toc;
end

