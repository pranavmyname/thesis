function M = create_slab_medium(N,slab_width,medium_para,para2,para3)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

epsilon_r = ones(N,1);  % 2 corner points have field = 0
epsilon_r(floor(N/2):floor(N/2)+slab_width-1) = medium_para; % Medium
if(nargin>3)
    epsilon_r(floor(N/2):floor(N/2)+slab_width-floor(slab_width/3)) = para2; % Medium
end
if(nargin>4)
    epsilon_r(floor(N/2)+floor(4*slab_width/5):floor(N/2)+slab_width-1) = para3; % Medium
end


mu_r = ones(N+1,1);     % Number of Magnetic points is N-1 as there are no
                        % initial values
% M_mu = diag(mu_r);

M = spdiags([epsilon_r; mu_r],0,2*N+1,2*N+1);

end

