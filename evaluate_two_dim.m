function [h_x,e,h_y,A] = evaluate_two_dim(P,Q,pml_length,omega_norma,q,L,plot_pml)
%UNTITLED2 Summary of this function doesn't go here
%   It evaluates

if(nargin<7)
    plot_pml = 0;
end

% P and Q are the number of separations on the dual grid and plus one on 
% the primary grid and number of points are one more than that.
% Step size calculation is done considering the primary grid where number
% of separationsa are N+1 and number of points are N+2 and therefore 
% step size is L/(N+2-1)

step_X = L/(P+1); step_Y = L/(Q+1);
% omega_norma = (c)^(-1)*L*omega;



%% D Matrix



Y_hat = magnetic_grid(step_Y,L,omega_norma,pml_length,Q,plot_pml);


% SInce steps in the primary grid are defined as y_q-y_{q-1} and the 
% steps go from delta_1 to delta_{q+1}, we need the end points as well

Y = primary_grid(step_Y,L,omega_norma,pml_length,Q,plot_pml);

X_hat = magnetic_grid(step_X,L,omega_norma,pml_length,Q,plot_pml);


X = primary_grid(step_X,L,omega_norma,pml_length,Q,plot_pml);

D_12 = kron(Y,speye(P));
D_21 = kron(Y_hat,speye(P));
D_23 = -kron(speye(Q),X_hat);
D_32 = -kron(speye(Q),X);


D = [sparse(size(D_12,1),size(D_21,2)) D_12 sparse(size(D_12,1),size(D_23,2)); ...
    D_21 sparse(size(D_21,1),size(D_12,2)) D_23; ...
    sparse(size(D_32,1),size(D_21,2)) D_32 sparse(size(D_32,1),size(D_23,2))];




C_mu_x = ones(P,Q+1);
C_mu_y = ones(P+1,Q);
C_epsilon = ones(P,Q);

% C_epsilon(ceil(P/3):ceil(2*P/3),ceil(P/3):ceil(2*P/3))=1;

M_comb = [C_mu_x(:);C_epsilon(:);C_mu_y(:)];

M = spdiags(M_comb,0,length(M_comb),length(M_comb));
A = M\D;

f = -(D+1i*omega_norma*M)\q;


h_x = f(1:P*(Q+1));
e = f(P*(Q+1)+1:P*(Q+1)+P*Q);
h_y = f(P*(Q+1)+1+P*Q:end);

end

