function Y_hat = magnetic_grid(step_Y,L,omega_norma,...
    pml_length,Q,plot_pml)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
Y_grid_hat = step_Y/2:step_Y:(L-step_Y/2);
Y_grid_hat = Y_grid_hat-Y_grid_hat(floor(end/2));
[region_mag] = create_pml(Y_grid_hat,pml_length,...
    L,omega_norma,plot_pml);
stretched_region_mag = Y_grid_hat./region_mag;
delta_Y = diff(stretched_region_mag);

% Y_hat = zeros(Q,Q+1)';
Y_hat = sparse([1:Q 1:Q],[1:Q 2:Q+1],[-1./delta_Y 1./delta_Y]);
% Y_hat(1:Q+2:end-1) = -1./delta_Y;
% Y_hat(2:Q+2:end) = 1./delta_Y;
% Y_hat = Y_hat';
end

