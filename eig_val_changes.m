%% Calculating change in the eigenvalues for different M's

M_para = 4;
sigma_range = 10:10:150;
[eig_va_change,eig_change_energy] = eig_val_derrivative(M_para,N,pml_length,omega...
    ,q,L,step_size,sigma_range,slab_width,c);

%%

figure()
plot(real(eig_va_change));

%%
figure()
scatter(real(eig_val),imag(eig_val),[],eig_change_energy);
colorbar

%%
scatter(real(eig_val(eig_change_energy)),imag(eig_val(eig_change_energy)))