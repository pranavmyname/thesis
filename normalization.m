clear; close all;




%%

[N,L,step_size,c,e_o,u_o,omega,Z_o,~,pml_length,...
    pulse_loc,slab_width,sigma,pml_strength] = get_constants();

lambda = (2*pi*3*10^8)/omega;

n_omega = 10;
X_dash = step_size:step_size:L-step_size;
X_dash_mag = step_size/2:step_size:(L-step_size/2);

omega_norma = (c)^(-1)*L*omega;


%% Gaussian Pulse

[omega_selected,t] = get_omega(omega,X_dash(pulse_loc));
omega_selected = omega;


%% Inputs
% Scaled impulse inputs

j_ext = zeros(N,1); j_ext(pulse_loc) = L*Z_o;
k_ext = zeros(N+1,1);
q = [j_ext ; k_ext];



%% Scaled medium parameters ( currently no dependence on distance and frequency)

M = create_slab_medium(N,slab_width,4);

%% Transfer Function

n_omega = length(omega_selected);
f_laplace = ones(n_omega,2*N+1);

f_time = zeros(length(t),2*N+1);


eig_val = zeros(n_omega,2*N+1);
eig_vec = zeros(2*N+1);
for i = 1:n_omega
    % Evaluating field vectors in the frequency domain
    [f_laplace(i,:),A,~,~,eig_val(i,:),eig_vec] =...
        evaluate_A_ind(N,pml_length,M,omega_selected(i),q,L,step_size...
        ,sigma,slab_width,c,pml_strength);
    % Inverse fourier transform for a unit impulse at some omega
    time_ifft = 2*exp(1i*omega*i*t');
    % Forming a matrix for inverse Fourier at all location for a given
    % Omega
    time_matrix = repmat(time_ifft,1,length(f_laplace));
    % Inverse Fourier transform of the field vectors
    f_time = f_time+1/sqrt(2*pi)*repmat(f_laplace(i,:)...
        ,length(t),1).*time_matrix;
end

