function [f_laplace,A,region_electric,region_mag,eig_val,eig_vec] =...
    evaluate(N,pml_length,M,omega,q,L,step_size,sigma, slab_width,c,pml_strength)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
e_o = 8.854*10^(-12); u_o = 4*pi*10^(-7);
Z_o = (u_o/e_o)^0.5;
omega_norma = (c)^(-1)*L*omega;

%% PML
% X_dash = linspace(0,L,N);
X_dash = step_size:step_size:L-step_size;
X_dash_mag = step_size/2:step_size:L-step_size/2;
% N = length(X_dash);

pml_range = X_dash(1:pml_length);
pml_range_mag = X_dash_mag(1:pml_length);

pml_area = 1 + (pml_strength*pml_range/L).^2/(1i*omega_norma);
pml_area_mag = 1 + (pml_strength*pml_range_mag/L).^2/(1i*omega_norma);

region_electric = [fliplr(pml_area) ones(1,N-2*pml_length) pml_area]; 
region_mag = [fliplr(pml_area_mag) ones(1,N-2*pml_length) pml_area_mag];


%% D Matrix


% This is wrong because pml value should be different for each
% index in the step_size i.e. different for y_{i-1} and y_i
% Fix it boi

Y_hat = zeros(N,N+1)';
Y_hat(1:N+2:end-1) = -(step_size)^(-1)./region_mag;
Y_hat(2:N+2:end) = (step_size)^(-1)./region_mag;
Y_hat = Y_hat';


Y = zeros(N+1,N)';
Y(1:N+1:end) = (1*step_size)^(-1)./region_electric;
Y(N+1:N+1:end) = -(1*step_size)^(-1)./[region_electric(2:end) region_electric(end)];
Y = Y';

D = [zeros(N,N) Y_hat; Y zeros(N+1,N+1)]*L;


%% Loss

M_sigma = zeros(size(M));
sigma = 1;
M_sigma(1:N,1:N) = L*Z_o*sigma;



%% Assign to base

assignin('base','D',D);


%% Output

tf_system = D+M_sigma+M*1i*omega_norma;
f_laplace = -tf_system\q;
A = M\D;
[eig_vec,eig_val] = eig(A);
eig_val = diag(eig_val);

end

