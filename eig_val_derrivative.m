function [eig_val_change,eig_change_energy] = eig_val_derrivative(M_para,...
    N,pml_length,omega,q,L,step_size,sigma,slab_width,c)
% Calculating change in the eigenvalues for different M's
%   Detailed explanation goes here

no_iterations = length(sigma);
M = create_slab_medium(N,slab_width,M_para(1));
eig_val_change = zeros(no_iterations-1,2*N+1);
[~,~,~,~,eig_val_prev] =...
    evaluate_A_ind(N,pml_length(1),M,omega,q,L,step_size,sigma(1),slab_width,c);
for i = 2:no_iterations
%     M = create_slab_medium(N,slab_width,M_para(i));
    [~,~,~,~,eig_val] =...
    evaluate_A_ind(N,pml_length,M,omega,q,L,step_size,sigma(i),slab_width,c);
    eig_val_change(i-1,:) = eig_val-eig_val_prev;
end
eig_change_energy = sum(abs(eig_val_change),1);
end

