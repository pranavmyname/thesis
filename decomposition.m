
%% Get Eigenvectors from the region of interest

min_eig = -5;
max_eig = -30;

minl = 150; maxl = 1500;

% I = (real(eig_val)<0 & real(eig_val)>-30 & imag(eig_val)<-5);
% I = ((real(eig_val)<5 & real(eig_val)>-20) & ((imag(eig_val)>-maxl ...
%     & imag(eig_val)<-minl) | (imag(eig_val)>minl & imag(eig_val)<maxl)));

I = ((real(eig_val)>0 & real(eig_val)>-20) & ((abs(imag(eig_val))<=maxl ...
    & abs(imag(eig_val))>=minl)));

eig_val_mod = eig_val(I);
eig_vec_mod = eig_vec(:,I);
ind_min = length(eig_val_mod);
ind_max = 1;
step_ind = 1;

omega_norm_mod = 18;


e_decomp = eig_vec_mod(:,ind_max:step_ind:ind_min)*diag(1./(eig_val_mod(ind_max:step_ind:ind_min)...
    +1i*omega_norma))*pinv(eig_vec_mod(:,ind_max:step_ind:ind_min))*(M\q);



figure();
subplot(2,2,[1,2]);
scatter(real(eig_val_mod),imag(eig_val_mod))
title(['Max eigen = ' num2str(maxl) ', Min Eigen = ' num2str(minl), ...
    ', Step Index = ' num2str(step_ind)]);
xlabel('Real(\lambda)');
ylabel('Imag(\lambda)');
subplot(2,2,3);
plot(abs(e_decomp(1:1000)));
title('Electric Field Absolute');
xlabel('Domain length in mm')
ylabel('Electric Field [a.u.]');
subplot(2,2,4);
plot(abs(e_decomp(1001:end)));
title('Magnetic Field Absolute');
xlabel('Domain length in mm')
ylabel('Electric Field [a.u.]');





%%  Plot Eigenvector
% 
figure;
% figure(scatt_eig);
figure();
plot(eig_val,'o');
title(['Eigenvalues for PML length ' num2str(pml_length)]);
xlabel('Real(\lambda)');
ylabel('Imag(\lambda)');    

%% Plot EigenValues

eig_val_plot = figure();
scatter(real(eig_val),imag(eig_val));
title(['Eigenvalues of the system matrix for slab width = ' num2str(slab_width)]);
xlabel('Real(\lambda)');
ylabel('Imag(\lambda)');


%% Interractive Plot

savefile = 0;
interractive_plot(eig_val,eig_vec,savefile);

%% Energy Profile EigenModes
energy_profile = zeros(size(eig_val));
% pml_length = 1;
for i = 1:length(eig_val)
    energy_profile(i) = energy_normal(eig_vec(:,i),pml_length,N);
%     imagesc(real(eig_val),imag(eig_val),energy_profile(i));
end
% energy_profile(1:500) = 1800;
figure()
scatter(real(eig_val),imag(eig_val),[],energy_profile);
title('Eigenvalues of the system matrix');
xlabel('Real(\lambda)');
ylabel('Imag(\lambda)');  
% grid on;
colorbar;
% figure()
% plot(energy_profile);


%% Plot changing sigmas

f_fig = figure;
% 
% scatter(real(eig_val),imag(eig_val));
% title(['Sigma = ' num2str(sigma)])
% xlim([0,inf]);
% grid on;

f_control = figure;
bgcolor = f_control.Color;
b_slider = uicontrol('Parent',f_control,'Style','slider','Position',[81,84,419,23],...
              'value',sigma, 'min',0, 'max',340);
          
sigma_label = uicontrol('Parent',f_control,'Style','text','Position',[240,57,100,23],...
                'String','Sigma','BackgroundColor',bgcolor);          
bl1 = uicontrol('Parent',f_control,'Style','slider', 'Position',[81,30,419,23],...
               'value',pml_length, 'min',0, 'max',330);  
pml_label = uicontrol('Parent',f_control,'Style','text','Position',[240,5,100,23],...
                'String','PML Length','BackgroundColor',bgcolor);  
pml_strength_bar = uicontrol('Parent',f_control,'Style','slider', 'Position',[81,134,419,23],...
               'value',pml_strength, 'min',0, 'max',330);   
pml_strength_label = uicontrol('Parent',f_control,'Style','text','Position',[240,110,100,23],...
                'String','PML Strength','BackgroundColor',bgcolor); 
slab_width_bar = uicontrol('Parent',f_control,'Style','slider', 'Position',[81,184,419,23],...
               'value',slab_width, 'min',10, 'max',330);     
slab_width_label = uicontrol('Parent',f_control,'Style','text','Position',[240,160,100,23],...
                'String','Slab Width','BackgroundColor',bgcolor);           
           
c_button = uicontrol('Parent',f_control,'Position',[10,30,60,50]);
c_button.String = 'Plot';
bg = uibuttongroup('Parent',f_control,'Position',[0 0.65 .13 0.5]);
noM_button = uicontrol(bg,'Style','radiobutton',...
                  'String','NoM',...
                  'Position',[10 110 70 30]);              
simpleM_button = uicontrol(bg,'Style','radiobutton','Position',...
    [10 80 70 30],'String','SimpleM');
doubleM_button = uicontrol(bg,'Style','radiobutton',...
                  'String','DoubleM',...
                  'Position',[10 50 70 30]);
tripleM_button = uicontrol(bg,'Style','radiobutton',...
                  'String','TripleM',...
                  'Position',[10 20 70 30]);
c_drop = uicontrol(f_control,'Style','popupmenu');
c_drop.Position = [10,90,60,50];
c_drop.String = {'0','20','40','60','80','100'}; 
N_drop = uicontrol(f_control,'Style','popupmenu');
N_drop.Position = [10,130,60,50];
N_drop.String = {'0','100','200','400','800','1000','2000'}; 
set(bg,'SelectedObject',simpleM_button);              
bl1.Callback = @(es,ed) disp(es.Value);
b_slider.Callback = @(es,ed) disp(es.Value);
slab_width_bar.Callback = @(es,ed) disp(es.Value);
% d_button.Callback = @(es,ed) disp(es.Value);
c_button.Callback = {@get_eig,b_slider, bl1,pml_strength_bar,slab_width_bar,bg,c_drop,N_drop,f_fig};         


%% Plot Arnlodi vs actual eigenvalues

b = M\q;
order_arnoldi = 500;
[Q,h] = arnoldi(A,b,order_arnoldi);
eig_arnoldi = eig(h);

figure();

scatter(real(eig_val),imag(eig_val))

xlabel('Real(\lambda)');
ylabel('Imag(\lambda)');

hold on
scatter(real(eig_arnoldi),imag(eig_arnoldi));
legend('Actual eigenvalues', 'Ritz Values');
title(['Eigen Value vs Ritz Values for order n = ' num2str(order_arnoldi)]);



%% RKS Decomposition

b = M\q;
order_arnoldi = 1000;
[Q,h] = arnoldi_RKS((A),b,order_arnoldi);
eig_arnoldi = eig(h);

figure();

scatter(real(eig_val),imag(eig_val));    

xlabel('Real(\lambda)');
ylabel('Imag(\lambda)');

hold on
scatter(real(eig_arnoldi),imag(eig_arnoldi));
legend('Actual eigenvalues', 'Ritz Values');
title(['Eigen Value vs Ritz Values for order n = ' num2str(order_arnoldi)]);

%% Plot RKS
% scatter(real(1./eig_arnoldi-1i*omega_norma),imag(1./eig_arnoldi-1i*omega_norma));
figure();
scatter(real(1./eig_arnoldi),imag(1./eig_arnoldi));
ylim([-2000,2000])
xlim([0,60])


%% Using Eigs function

[eig_vec_eigs,eig_eigs] = eigs(A+1i*omega_norma*speye(length(A)),500,'smallestabs');

plot(diag(eig_eigs),'o');
eigs_decomp = eig_vec_eigs*diag(1./(diag(eig_eigs)))...
    *pinv(eig_vec_eigs)*(M\q);
figure()
plot(abs(eigs_decomp));




