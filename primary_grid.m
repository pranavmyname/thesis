function Y = primary_grid(step_Y,L,omega_norma,...
    pml_length,Q,plot_pml)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% Since steps in the primary grid are defined as y_q-y_{q-1} and the 
% steps go from delta_1 to delta_{q+1}, we need the end points as well
Y_grid = (0:step_Y:L);
Y_grid = Y_grid-Y_grid(floor(end/2));
[region_elec] = create_pml(Y_grid,pml_length,...
    L,omega_norma,plot_pml);
stretched_region = Y_grid./region_elec;
delta_Y = diff(stretched_region);

Y = sparse([1:Q 2:Q+1],[1:Q 1:Q],[1./delta_Y(1:end-1) -1./delta_Y(2:end)]);
% Y = zeros(Q+1,Q)';
% Y(1:Q+1:end) = 1./delta_Y(1:end-1);
% Y(Q+1:Q+1:end) = -1./delta_Y(2:end);
% Y = Y';
end

