function scat_poles = scattering_poles(Y_1,sigma,e_2,u_o,d)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
syms s;
Y_2 = ((sigma+s*e_2)/(s*u_o))^0.5;
gamma_2 = ((sigma+s*e_2)*(s*u_o))^(0.5);
sys_fun =@(s) (1-(((Y_1-Y_2)/(Y_1+Y_2))^2)*exp(-2*gamma_2*d));
scat_poles = fsolve([real(sys_fun(s)), imag(sys_fun(s))],4);
end

