% clear;

 [N,L,step_size,c,e_o,u_o,omega,Z_o,~,~,pulse_loc,slab_width,sigma]...
     = get_constants();
omega_norma = (c)^(-1)*L*omega;
% omega = pi*1e+14;
sigma = 0;
sigma_slab = sigma/(L*Z_o);
%  N = 500;
%  step_size = L/(N+1);
X_dash = linspace(0,L,N);
%   slab_width = 30; 
%   pulse_loc = 100;
% sigma = sigma/(L*Z_o);

%% Medium and space

epsilon_r = ones(size(X_dash));%.*region_electric; 
start_to_source = 1:(pulse_loc-1);
incident_space = pulse_loc:(floor(N/2)-1);
medium = floor(N/2):(floor(N/2)+slab_width-1);
transmitted_space = floor(N/2)+slab_width:length(X_dash);
epsilon_r(medium) = 4; % Medium
mu = 1;

%% Parameters

% J_ext = 
I_del = L*1e-3;
% Should be actually start to the medium
source_to_med = (floor(N/2))*(L/(N-1));
% source_to_med = (floor(N/2))/(N-1);
gamma_1 = 1i*omega*sqrt(e_o*u_o);
% gamma_1 = 1i*omega_norma;
gamma_2 = sqrt(1i*omega*(sigma_slab+1i*omega*max(epsilon_r)*e_o)*u_o);
% gamma_2 = sqrt(1i*omega_norma*(sigma+1i*omega_norma*max(epsilon_r)));
Y_1 = 1/Z_o;
% Y_1 = 1;
Y_2 = (sigma_slab+1i*omega*max(epsilon_r)*e_o)/gamma_2;
% Y_2 = (sigma+1i*omega_norma*max(epsilon_r))/gamma_2;
% Y_2 = sqrt(max(epsilon_r))*1/Z_o;

% Electric Field at ths start of medium due to the source at pulse_loc
% +1 because X_dash starts from 1 {X_dash(2) = 1*L/(N-1)}, so
% -(pulse_loc-1)
e_1i_left = 1/(2*Y_1)*I_del*exp(gamma_1*(floor(N/2)-pulse_loc+1)*L/(N-1));
e_1i = 1/(2*Y_1)*I_del*exp(-gamma_1*(floor(N/2)-pulse_loc+1)*L/(N-1));

d = length(floor(N/2):floor(N/2)+slab_width-1)*step_size;
A_plus = (2*Y_1/(Y_1+Y_2))/(1-((Y_1-Y_2)/(Y_1+Y_2))^2*exp(-2*gamma_2*d));
A_minus= (-2*Y_1*(Y_1-Y_2)/(Y_1+Y_2)^2*exp(-2*gamma_2*d))/...
    (1-((Y_1-Y_2)/(Y_1+Y_2))^2*exp(-2*gamma_2*d));

% R is the reflection coefficient
R = ((Y_1-Y_2)/(Y_1+Y_2))*(1-exp(-2*gamma_2*d))/...
    (1-((Y_1-Y_2)/(Y_1+Y_2))^2*exp(-2*gamma_2*d));
% T is the reflection coefficient
T = 4*Y_1*Y_2/(Y_1+Y_2)^2*exp(gamma_1*d-gamma_2*d)/...
    (1-((Y_1-Y_2)/(Y_1+Y_2))^2*exp(-2*gamma_2*d));

%% Calculating Field

% E_medium = e_1i*(A_plus*exp(-gamma_2*(X_dash-X_dash(floor(N/2))))+...
%     A_minus*exp(gamma_2*(X_dash-X_dash(floor(N/2)))));

E_left = e_1i_left*exp(gamma_1*(X_dash(start_to_source)-source_to_med));

E_incident = e_1i*exp(-gamma_1*(X_dash(incident_space)-source_to_med));

E_medium = e_1i*(A_plus*exp(-gamma_2*(X_dash(medium)-source_to_med))+...
    A_minus*exp(gamma_2*(X_dash(medium)-source_to_med)));


E_reflected = R*e_1i*exp(gamma_1*(X_dash(incident_space)-source_to_med));

E_transmitted = T*e_1i*exp(-gamma_1*(X_dash(transmitted_space)-source_to_med));
slab_analytic_fig = figure;
figure(slab_analytic_fig);
subplot(3,1,1);
plot([real(E_left) (real(E_reflected+E_incident)) real(E_medium) real(E_transmitted)]);
title('Real(Electric Field)');
xlabel('Domain length in mm')
ylabel('Electric Field [a.u.]');
subplot(3,1,2);
plot([imag(E_left) (imag(E_reflected+E_incident)) imag(E_medium) imag(E_transmitted)]);
title('Imaginary(Electric Field)');
xlabel('Domain length in mm')
ylabel('Electric Field [a.u.]');
subplot(3,1,3);
plot([abs(E_left) (abs(E_reflected+E_incident)) abs(E_medium) abs(E_transmitted)]);
title('Absolute Electric Field');
xlabel('Domain length in mm')
ylabel('Electric Field [a.u.]');

%% Scattering poles

n = -1000:1000;
K = ((Y_1-Y_2)/(Y_1+Y_2))^2*ones(size(n));
m = 2*sqrt(4)/c*d;
omega_s = (-log(K)/m+1i*2*n*pi/m)/c*L;
try
%     figure(eig_val_plot);
    figure(f_fig);
catch
    warning('Eigenvalue plot does not exist, creating a new one');
    figure();
end
hold on
scatter(real(omega_s),imag(omega_s));
title('Scattering Poles of the system matrix');
% legend('Eigenvalue of system matrix',...
%     ['Scattering poles of the analytical equation for slab width '...
%     num2str(slab_width)]);
xlabel('Real axis (Real(\lambda))');
ylabel('Imaginary axis (Im(\lambda))');
% legend('Eigenvalue of system matrix',...
%     ['Scattering poles of the analytical equation for N = '...
%     num2str(N)]);

% legend('Eigenvalue of system matrix',...
%     'Scattering poles of the analytical equation');
% title('Scattering poles of the analytical equation');
grid on;
 ylim([-2*N,2*N]);