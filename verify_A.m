[~,~,~,c,~,~,omega] = get_constants();
E_x = exp(-1i*omega/c*abs((X_dash-X_dash(500))))/(2*1i*omega/c);
verify_field = figure;
figure(verify_field);
subplot(3,1,1);
plot(real(E_x));
title('Real part of E')
subplot(3,1,2);
plot(imag(E_x));
title('Imaginary part of E');
subplot(3,1,3);
plot(abs(E_x));
title('Absolute E');


%% Slab Implementation



