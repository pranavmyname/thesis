function get_cord_real(aH,evnt,eig_val,eig_vec)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
[x,y] = ginput(1);
f = ancestor(aH,'figure');
click_type = get(f,'SelectionType');

if strcmp(click_type,'normal')
% pt = get(aH,'CurrentPoint');

pt = get(aH,'Xdata');
N = floor((length(eig_val)-1)/2);
[k_dist,k_ind] = min(abs(pt-x(1)))
if(k_dist<20)   
    figure(10);
    plot(real(eig_vec(:,k_ind(1))))
    uiresume(gcf);
else
    close all
end
end

