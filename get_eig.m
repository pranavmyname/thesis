function eig_val = get_eig(src,event,b_slider, bl1,pml_strength_bar,...
    slab_width_bar, bg,c_drop,N_drop, f_fig)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% sigma = src.Value;
sigma = floor(b_slider.Value);
pml_length = floor(bl1.Value);
pml_strength = floor(pml_strength_bar.Value);
slab_width = floor(slab_width_bar.Value);
disp(['Sigma = ' num2str(sigma)])
disp(['PML length = ' num2str(pml_length)]);
disp(['PML Strength = ' num2str(pml_strength)]);
disp(['Slab WIdth = ' num2str(slab_width)]);
[N,L,step_size,c,~,~,omega,Z_o,~,~,...
    pulse_loc,~] = get_constants();
N_check = str2num(N_drop.String{N_drop.Value});
if(N_check)
    N = N_check;
end
M_status = get(get(bg,'SelectedObject'),'String');
if(strcmp(M_status, 'NoM'))
    M = speye(2*N+1);
elseif (strcmp(M_status, 'SimpleM'))
    M = create_slab_medium(N,slab_width,4);
elseif (strcmp(M_status, 'DoubleM'))
    M = create_slab_medium(N,slab_width,4,7);
else
    M = create_slab_medium(N,slab_width,4,7,2.6);
end
j_ext = zeros(N,1); j_ext(pulse_loc) = L*Z_o;
k_ext = zeros(N+1,1);
q = [j_ext ; k_ext];

[~,~,~,~,eig_val,eig_vec] =...
    evaluate_A_ind(N,pml_length,M,omega,q,L,step_size,sigma,slab_width,c,pml_strength);
% figure(f_fig);
% scatter(real(eig_val),imag(eig_val));
% title(['Sigma = ' num2str(sigma)])
% xlim([0,inf]);
% grid on;
savefile = 0;
x_lim = str2num(c_drop.String{c_drop.Value});
interractive_plot(eig_val,eig_vec,savefile,f_fig,x_lim);
end

