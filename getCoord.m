function getCoord(aH,evnt,eig_val,eig_vec,savefile)
drawnow
f = ancestor(aH,'figure');
click_type = get(f,'SelectionType');
ptH = getappdata(aH,'CurrentPoint');
% delete(ptH)
% if strcmp(click_type,'normal')
% elseif strcmp(click_type,'alt')
%     %do your stuff once your point is selected   
%     disp('Done clicking!');
%     % HERE IS WHERE YOU CAN PUT YOUR STUFF
figure(10);
if strcmp(click_type,'normal')
pt = get(aH,'CurrentPoint');
N = floor((length(eig_val)-1)/2);
% k_ind = find((abs(real(eig_val)-pt(1))<9e-1 & abs(imag(eig_val)-pt(3))<9e-1))
% [k_dist,k_ind] = min(abs(eig_val-(pt(1)+1i*pt(3))));
real_range = max(real(eig_val))-min(real(eig_val));
imag_range = max(imag(eig_val))-min(imag(eig_val));
[k_dist,k_ind] = min(abs(real(eig_val)-pt(1)+...
    1i*real_range/(imag_range)*(imag(eig_val)-pt(3))))
  
%     plot(real(eig_vec(:,k_ind(1))))
    
    if(imag(eig_val(k_ind))<0)
        title_str = [' Eigenmode corrresponding to ' ...
        num2str(real(eig_val(k_ind))) num2str(imag(eig_val(k_ind))) 'i'];
    else
        title_str = [' Eigenmode corrresponding to ' ...
        num2str(real(eig_val(k_ind))) '+' num2str(imag(eig_val(k_ind))) 'i'];
    end
    
    subplot(3,1,1);
    
    plot(real(eig_vec(1:N,k_ind(1))))
    grid on
    xlim([0,N]);
    title(['Real(Electrical)' title_str]);
    xlabel('Domain length in mm')
    ylabel('Electric Field [a.u.]');

    subplot(3,1,2);
    plot(imag(eig_vec(1:N,k_ind(1))))
    grid on
    xlim([0,N]);
    title(['Imag(Electrical)' title_str]);    
    xlabel('Domain length in mm')
    ylabel('Electric Field [a.u.]');

    subplot(3,1,3);
    plot(abs(eig_vec(1:N,k_ind(1))))
    grid on
    title(['Absolute(Electrical)' title_str]);
    xlabel('Domain length in mm')
    ylabel('Electric Field [a.u.]');
    xlim([0,N]);
    if(savefile)
        saveas(gcf,['Eigen_modes_slab_width/eig_vec_electrical' num2str(round(real(eig_val(k_ind)))) '_' ...
            num2str(round(imag(eig_val(k_ind))))],'epsc');
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    figure(11)
    subplot(3,1,1);
    plot(real(eig_vec(N+1:2*N+1,k_ind(1))))
    grid on
    title(['Real(Magnetic)' title_str]);
    xlabel('Domain length in mm')
    ylabel('Magnetic Field [a.u.]');
    xlim([0,N]);
    fft_eig = fft(eig_vec(:,k_ind(1)));
    
    subplot(3,1,2);
    plot(imag(eig_vec(N+1:2*N+1,k_ind(1))))
    grid on
    title(['Imag(Magnetic)' title_str]);
    xlabel('Domain length in mm')
    ylabel('Magnetic Field [a.u.]');
    xlim([0,N]);
    
    

    
    subplot(3,1,3);
    plot(abs(eig_vec(N+1:2*N+1,k_ind(1))))
    grid on
    title(['Absolute(Magnetic)' title_str]);
    xlabel('Domain length in mm')
    ylabel('Magnetic Field [a.u.]');
    xlim([0,N]);
    
    if(savefile)
        saveas(gcf,['Eigen_modes_slab_width/eig_vec_mag_' num2str(round(real(eig_val(k_ind)))) '_' ...
            num2str(round(imag(eig_val(k_ind))))],'epsc');
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    figure(13);
    plot(abs(fftshift(fft_eig)));
    grid on
    uiresume(gcf);
   
end