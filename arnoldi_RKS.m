function [V,h] = arnoldi_RKS(A,b,n,omega_norma)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if(nargin<3)
    n = 500;
end
if(nargin<4)
    [~,L,~,c,~,~,omega] = get_constants();
    omega_norma = (c)^(-1)*L*omega;
end

sr = omega_norma*linspace(-1,0,n);
v = b/norm(b);
V = zeros(length(A),n);
V(:,1) = v;
h = zeros(n);
for i = 1:n
%     p = A_kr*V(:,i-1);
%     p = (A+1i*omega_norma*speye(length(A)))\v;
    p = A\v;
    for j = 1:i-1
        h(j,i-1) = p'*V(:,j);
        p = p - (p'*V(:,j))*V(:,j);
    end
    if(i>1)
        h(i,i-1) = norm(p);
    end
    v = p/norm(p);
    V(:,i) = v;
end
% h = V'*A_kr*V;
end

