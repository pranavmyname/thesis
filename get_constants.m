function [N,L,step_size,c,e_o,u_o,omega,Z_o,lambda,pml_length,...
    pulse_loc,slab_width,sigma,pml_strength] = get_constants(omega)
% This is where you set the constants

if(nargin<1)
    omega = 4.5*10^(14);
end
e_o = 8.854*10^(-12); u_o = 4*pi*10^(-7);
N = 1000;
c = 3*10^8;
lambda = (2*pi*c)./omega;


L = 20*lambda;    %3e-6;   % 5*lambda


% N the number of separations on the dual grid and plus one on the
% primary grid and number of points is one more than that.
% Step size calculation is done considering the primary grid where number
% of separationsa are N+1 and number of points are N+2 and therefore 
% step size is L/(N+2-1)

step_size = L/(N+1);
slab_width = floor(10*(L/100)/step_size);
% slab_width = 100;

e_o = 8.854*10^(-12); u_o = 4*pi*10^(-7);
Z_o = (u_o/e_o)^0.5;
pml_length = floor(L/(8*step_size));
pulse_loc = floor(L/(4*step_size)+N/10);
sigma = 0;
pml_strength = 60;
end

