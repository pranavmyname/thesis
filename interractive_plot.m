function scatt_eig = interractive_plot(eig_val,eig_vec,savefile,f_fig,x_lim)
if(nargin>3)
    figure(f_fig);
    clf
else
    figure;
end

% figure(scatt_eig);
eig_val_plot = axes;

if(isdiag(eig_val))
    eig_val = diag(eig_val);
end

hold on

if(isreal(eig_val))
    plot_eig = plot(eig_val,'o');
    
    set(plot_eig,'ButtonDownFcn',{@get_cord_real,eig_val,eig_vec});
    uiwait(gcf) %so multiple clicks can be used
else
    
    scatt_eig = scatter(real(eig_val),imag(eig_val));
    title(['Eigenvalues of the system matrix']);
    xlabel('Real(\lambda)');
    ylabel('Imag(\lambda)');
    grid on
    if(savefile)
        saveas(gcf,'Eigen_modes_slab_width/eig_val','epsc');
    end
    if(nargin>4)
        if(x_lim>0)
            xlim([0,x_lim]);
        end
    end
    
    set(scatt_eig,'HitTest','off'); % so you can click on the Markers
    hold on;
    % Defining what happens when clicking
    set(eig_val_plot,'ButtonDownFcn',{@getCoord,eig_val,eig_vec,savefile});
    uiwait(gcf) %so multiple clicks can be used
end
end

