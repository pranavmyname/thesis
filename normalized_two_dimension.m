clear;
omega = 1*pi*10^(14);
lambda = (2*pi*3*10^8)/omega;
P = 100;
Q = 100;
pml_length = 20;
c = 3*10^8;
L = 10*lambda;
e_o = 8.854*10^(-12); u_o = 4*pi*10^(-7);
Z_o = (u_o/e_o)^0.5;
omega_norma = (c)^(-1)*L*omega;
q = [sparse(P*(Q+1),1);sparse(P*Q,1);sparse((P+1)*Q,1)];
q(P*(Q+1)+floor(P*Q/2)+floor(Q/2)) = L*Z_o;

[h_x,e,h_y,A] = evaluate_two_dim(P,Q,pml_length,omega_norma,q,L);


e_r = reshape(e,[P,Q]);
figure();
surf(imag(e_r));
h_x_r = reshape(h_x,[P,Q+1]);
h_y_r = reshape(h_y,[P+1,Q]);
% 
% 
% 
%% Plot

figure();
subplot(3,1,1)
plot(real(h_x))
title('H_x');
subplot(3,1,2)
plot(abs(e))
title('e');
subplot(3,1,3)
plot(real(h_y))
title('H_y');