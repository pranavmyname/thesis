%% Plotting eigenvalues of the inverse of (A+sI)

A_inv = inv(A+1i*omega_norma*speye(size(A)));

[eig_vec_A_inv,eig_A_inv] = eigs(A,2*N+1);
eig_A_inv = diag(eig_A_inv);

%% Interractive Plot

savefile = 0;
interractive_plot(eig_A_inv,eig_vec_A_inv,savefile);