function [omega_selected,t] = get_omega(omega,pulse_loc)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
%   Inputs: 
%       omega: Central frequency omega
%       pulse_loc: To plot the frequencies selected


time_step = 1/(20*omega);
t = (0:time_step:10*(1/omega));



if (nargin==2)
    sigma = 3*time_step;
    Gaussian=exp(-(t-t(floor(length(t)/11))).^2/(2*sigma^2));
    y = diff(Gaussian);%/step_size;%.*exp(-1i*omega*4*X_dash(2:end));
    y_fft = fft(y)/length(t);
    omega_selected_amplitude = abs(y_fft(4:4:30));
    figure;
    subplot(2,1,1)
    plot(t(1:end-1),real(y))
    title(['Pulse at location x = ' num2str((pulse_loc))]);
    xlabel('Time');
    ylabel('Electric Current Density [a.u.]');
    subplot(2,1,2)
    plot((-length(y_fft)/2:length(y_fft)/2-1)/...
        (length(y)*time_step),fftshift(abs(y_fft)));
    hold on;
    stem((4:4:30)/(length(y)*time_step),omega_selected_amplitude);
    title('Fourier transform of the pulse');
    legend('Fourier transform', 'Frequencies Selected');
    xlabel('Frequency (Hz)');
    ylabel('Amplitude');
end


omega_selected = (4:4:30)/((length(t)-1)*time_step);


end

