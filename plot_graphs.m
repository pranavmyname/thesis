
[~,~,region_electric,region_mag] = evaluate_A_ind(N,pml_length,M,omega_selected(i),q,L,step_size...
        ,sigma,slab_width,c,pml_strength);
% [~,region_electric,region_mag] = evaluate(N,pml_length,M,omega,q,L);
figure(1);
subplot(2,1,1);
plot(-imag(region_electric));
title('Imaginary part of the Stretching function');
xlabel('Number of discretization points');
ylabel('Im(\alpha y^2/(j\omega)');
grid on

subplot(2,1,2);
plot(abs(region_electric));
title('Absolute Stretching function');
xlabel('Number of discretization points');
ylabel('|1+\alpha y^2/(j\omega)|');
grid on


%%

f_laplace1 = f_laplace(1,:);
E = f_laplace1(1:N);
H = f_laplace1(N+1:2*N+1);
% H = [f_laplace(N+1:N+500) ;-f_laplace(N+501:end)];

figure();
% plot((ifft(f_laplace)));
subplot(3,2,1);
plot(real(E));
title('Electric Field Real');
xlabel('Domain length in mm')
ylabel('Electric Field [a.u.]');
subplot(3,2,2);
plot(real(H));
xlim([0,N]);
title('Magnetic Field Real');
xlabel('Domain length in mm')
ylabel('Magnetic Field [a.u.]');
subplot(3,2,3);

plot(imag(E));
title('Electric Field Imag');
xlabel('Domain length in mm')
ylabel('Electric Field [a.u.]');
subplot(3,2,4);
plot(imag(H));
xlim([0,N]);
title('Magnetic Field Imag');
xlabel('Domain length in mm')
ylabel('Magnetic Field [a.u.]');
subplot(3,2,5);
plot(abs(E));
title('Electric Field Absolute');
xlabel('Domain length in mm')
ylabel('Electric Field [a.u.]');
subplot(3,2,6);
plot(abs(H));
xlim([0,N]);
title('Magnetic Field Absolute');
xlabel('Domain length in mm')
ylabel('Magnetic Field [a.u.]');


%% Saving images for different M

figure()
% no_samples = 10;
N = [1000,2000];
for i = 1:length(N)
%     medium_para = 1+(50/(no_samples-1))*(i-1);
%     M = create_slab_medium(N,slab_width,medium_para);
    [~,~,~,~,eig_val] =...
        evaluate_A_ind(N(i),pml_length,M,omega,q,L,step_size,sigma,slab_width,c);
    plot(eig_val,'o');
    title(['Eigenvalues for Medium Parameter ' num2str(medium_para)]);
    xlabel('Real(\lambda)');
    ylabel('Imag(\lambda)');
    xlim([0,20]);
    drawnow
    frame = getframe(gcf);
    im{i} = frame2im(frame);
    saveas(gcf,['eig_val_vary_M/eig_vec_M_' num2str(i) '.png']);
end
% filename = 'eig_val_vary_M/eig_vec_M.gif';
% create_animation(im,filename,no_samples);

%% Saving Images for different pml_length
clear im
figure()
no_samples = 10;
for i = 1:no_samples
    pml_strength_range = floor((200/(no_samples-1))*(i-1));
    [~,~,~,~,eig_val] =...
        evaluate_A_ind(N,pml_length,M,omega,q,L,step_size,sigma,slab_width,c,pml_strength_range);
    plot(eig_val,'o');
    title(['Eigenvalues for PML Strength ' num2str(pml_strength_range)]);
    xlabel('Real(\lambda)');
    ylabel('Imag(\lambda)');
    xlim([0,inf]);
    drawnow
    frame = getframe(gcf);
    im{i} = frame2im(frame);
    saveas(gcf,['eig_val_vary_pmlLength/eig_vec_pml_' num2str(i) '.png']);
end
% save('eig_val_vary_pmlLength/varying_pml_data.mat','im');
% filename = 'eig_val_vary_pmlLength/eig_vec_pml.gif';
% create_animation(im,filename,no_samples);


%% Saving Images for different Sigma
clear im
figure()
no_samples = 10;
for i = 1:no_samples
    sigma = floor((100/(no_samples-1))*(i-1));
    [~,~,~,~,eig_val] =...
        evaluate_A_ind(N,pml_length,M,omega,q,L,step_size,sigma,slab_width,c);
    plot(eig_val,'o');
    title(['Eigenvalues for Sigma ' num2str(sigma)]);
    xlabel('Real(\lambda)');
    ylabel('Imag(\lambda)');
    xlim([0,20]);
    drawnow
    frame = getframe(gcf);
    im{i} = frame2im(frame);
    saveas(gcf,['eig_val_vary_M/eig_vec_M_' num2str(i) '.png']);
end
% save('eig_val_vary_pmlLength/varying_pml_data.mat','im');
% filename = 'eig_val_vary_pmlLength/eig_vec_pml.gif';
% create_animation(im,filename,no_samples);

%% Plot Amimation

figure(3);
for i = 1:length(t)
    subplot(2,1,1)
    plot(real(f_time(i,1:N)));
    ylim([-4e-5 4e-5])
    xlim([0 N])
    title('Electric Field'); 
    subplot(2,1,2)
    plot(real(f_time(i,N+1:end)));
    ylim([-4e-5 4e-5])
    xlim([0 N+1])
    title('Magnetic Field');
    pause(1/10);    
end


%%
figure(3);
subplot(3,1,1);
plot(abs(ifft(f_laplace)));
title('Absolute inverse f laplace');
subplot(3,1,2);
plot(real(ifft(f_laplace)));
title('Real inverse f laplace');
subplot(3,1,3);
plot(imag(ifft(f_laplace)));
title('Imaginary inverse f laplace');

%% 

figure(4);
subplot(2,1,1);
plot(abs(ifft(E)));
title('Absolute Electric field');
subplot(2,1,2);
plot(abs(ifft(H/Z_o)));
title('Absolute Magnetic field');
axis tight